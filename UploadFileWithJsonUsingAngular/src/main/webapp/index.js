var myApp = angular.module('myApp', ['ngRoute']);
myApp.service('fileUploadService', ['$http', function ($http) {
    this.files = [];
    this.uploadFileToUrl = function (uploadUrl) {
        $http(
            {
                method: "POST",
                url: uploadUrl,
                transformRequest: function (data) {
                    var fd = new FormData();
                    for (var i = 0; i < data.files.length; i++) {
                        fd.append('file' + (i + 1), data.files[i]);
                    }
                    fd.append("jsonData", data.model);
                    return fd;
                },
                headers: {'Content-Type': 'multipart/form-data'},
                data: {
                    model: "{'test': 'jdjdjdjddjdj'}",
                    files: this.files
                }
            });
    }
}]);

myApp.directive('fileUpload', function () {
    return {
        scope: true,
        link: function (scope, el) {
            el.bind('change', function (event) {
                for (var i = 0; i < event.target.files.length; i++) {
                    scope.$emit("fileSelected", {file: event.target.files[i]});
                }
            });
        }
    };
});


myApp.controller('Home', ['$scope', 'fileUploadService', function ($scope, fileUploadService) {

    $scope.$on("fileSelected", function (evt, args) {
        $scope.$apply(function () {
            fileUploadService.files.push(args.file);
            $scope.files = fileUploadService.files;
        });
    });
}]);


myApp.controller('Next', ['$scope', 'fileUploadService', function ($scope, fileUploadService) {

    $scope.uploadFile = function () {
        var uploadUrl = "/webapp/fuck/upload";
        fileUploadService.uploadFileToUrl(uploadUrl);
    };
}]);