angular.module('myApp').config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl : 'home.html',
            controller : 'Home'
        })
        .when('/next',{
            templateUrl : 'next.html',
            controller : 'Next'
        });
}]);